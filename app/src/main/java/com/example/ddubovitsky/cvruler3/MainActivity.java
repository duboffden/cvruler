package com.example.ddubovitsky.cvruler3;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.aruco.Aruco;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;

    private static double MARKER_SIDE_MM = 51.3;

    private static final String CAMERA_PERMISSION = Manifest.permission.CAMERA;

    private static final int PERMISSIONS_REQUEST = 11;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    checkPermissions();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                CAMERA_PERMISSION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
        } else
            mOpenCvCameraView.enableView();
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA_PERMISSION}, PERMISSIONS_REQUEST);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mOpenCvCameraView = findViewById(R.id.jcv_output);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat rgba = inputFrame.rgba();

//        Mat gray = inputFrame.gray();
//
//        ArrayList<Mat> corners = new ArrayList<>();
//        Mat ids = new Mat();
//        Aruco.detectMarkers(gray, Aruco.getPredefinedDictionary(Aruco.DICT_ARUCO_ORIsGINAL), corners, ids);
//
//        if (corners.isEmpty()) {
//            return rgba;
//        }
//
//        Mat corner = corners.get(0);
//        double[] firstXy = corner.get(0, 0);
//        double[] secondXy = corner.get(0, 1);
//        double diffX = firstXy[0] - secondXy[0];
//        double diffY = firstXy[1] - secondXy[1];
//
//        double markerSidePx = Math.sqrt(diffX * diffX + diffY * diffY);
//
////        Log.d("+++", "diff x " + diffX + " diff y " + diffY);
////        Log.d("+++", "marked side px = " + markerSidePx);
//
//        double scaleFactor = MARKER_SIDE_MM / markerSidePx;
//
//        Log.d("+++", "scale factor = " + scaleFactor);
//        ArrayList<Point> centers = new ArrayList<>();
//
//        for (Mat marker : corners) {
//            Point center = getCenter(marker);
//            centers.add(getCenter(marker));
//            Imgproc.circle(rgba, center, 4, red, -1);
//        }
//
//        for (int i = 0; i < centers.size(); i++) {
//            for (int j = i + 1; j < centers.size(); j++) {
//                Point p1 = centers.get(i);
//                Point p2 = centers.get(j);
//
//                double distance = getDistance(p1, p2, scaleFactor);
//                Log.d("+++", "distance = " + distance);
//                Imgproc.putText(rgba, (int) distance + "mm", getCenter(p1, p2), Core.FONT_HERSHEY_COMPLEX, 1, red, 2);
//                Imgproc.line(rgba, p1, p2, red);
//            }
//        }

//        Log.d("+++", "centers " + centers);

        return rgba;
    }

    Scalar red = new Scalar(255, 0, 0);

    private Point getCenter(Mat marker) {

        double sumX = 0;
        double sumY = 0;

        for (int i = 0; i < 4; i++) {
            double[] xy = marker.get(0, i);
            sumX += xy[0];
            sumY += xy[1];
        }

        return new Point(sumX / 4, sumY / 4);
    }

    private double getDistance(Point p1, Point p2, double scaleFactor) {
        double diffX = p1.x - p2.x;
        double diffY = p1.y - p2.y;
        double sum = diffX * diffX + diffY * diffY;
        return Math.sqrt(sum) * scaleFactor;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private Point getCenter(Point p1, Point p2) {
        double midX = (p1.x + p2.x) / 2;
        double midY = (p1.y + p2.y) / 2;
        return new Point(midX, midY);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkPermissions();
    }
}
